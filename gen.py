#!/usr/bin/env python3
import random
import essential_generators as eg
import sys, os
import string
import docx
import xlsxwriter
import bz2, gzip, lzma, zipfile, tarfile

m = eg.MarkovTextGenerator()
w = eg.MarkovWordGenerator()
d = eg.DocumentGenerator()

#Where to start the files from
base_path='dir'

#Zipf distribution is that probability is proportional to 1/size.
#So we need a cofactor of that (Smaller = bigger distribution and fiewer files)
lambd=0.7

#We also need a max size
global_max_size = 200 #MiB
max_size = 100 #MiB
#and a file number to generate
#file_count = 400
#Do we want both text and binary?
types=['DOCUMENT','CODE','BINARY'] #'ARCHIVE']

def gen_name(max_len=15):
    name='.'
    while not name.isalpha():
        name = w.gen_word(max_len)
    return name

def gen_docx(filename, size, header_length=20,document_length=None):
    filename = '{}.docx'.format(filename)
    document = docx.Document()
    document.add_heading(m.gen_text(header_length), 0)
    if document_length is None:
        document_length = int(size * 1024 * 1024 / 4) #Input is MiB
    document.add_paragraph(m.gen_text(document_length))
    document.save(filename)
    return os.path.getsize(filename)/1024/1024 #Output is MiB

def gen_xlsx(filename, size):
    filename = '{}.xlsx'.format(filename)
    template = {
      0: 'name',
      1: 'upc',
      2: 'small_int',
      3: 'integer',
      4: 'floating',
      5: 'url',
      6: 'guid',
      7: 'sentence',
    }
    d.set_template(template)
    entries = d.documents(int(size*1024*1024/80)) #Estimate
    workbook = xlsxwriter.Workbook(filename)
    worksheet = workbook.add_worksheet()
    for n, e in enumerate(entries):
       for r in e:
           worksheet.write(n, r,e[r])
    workbook.close()
    return os.path.getsize(filename)/1024/1024 #Output is MiB

def gen_text(filename, size, extension='txt'):
    filename = '{}.{}'.format(filename,extension)
    with open(filename,'w') as f:
        f.write(m.gen_text(int(size*1024*1024/4)))
    return os.path.getsize(filename)/1024/1024 #Output is MiB

def gen_code(filename, size):
    extension = random.choice(['py','c','h','js','rb','go','cpp'])
    return gen_text(filename, size, extension)

def gen_binary(filename, size,type=None):
    extension = random.choice(['','.dat','.data'])
    data = random.randbytes(int(max(0,(size*1024*1024)-116))) #"Contents"
    filename = '{}{}'.format(filename,extension)
    with open(filename,'wb') as f:
        f.write(data)
    return os.path.getsize(filename)/1024/1024 #Output is MiB

def gen_exe(filename, size,type=None):
    if type is None:
        type = random.choice(['lin','win'])
    if type == 'lin':
        extension = random.choice(['','.ko','.so'])
        data = bytes.fromhex('7f454c4c') #ELF Magic
        data += bytes.fromhex(random.choice(['01','02'])) #32/64 bit
        data += bytes.fromhex(random.choice(['01','02'])) #Little/Big endian
        data += bytes.fromhex('01') #Version
        data += random.randbytes(1) #OS ABI
        data += bytes.fromhex('00') #ABI Version
        data += bytes.fromhex('00') #Pad
        data += random.randbytes(int(max(0,(size*1024*1024)-10))) #"Contents"
    elif type == 'win':
        extension = random.choice(['.exe','.dll'])
        data  = bytes.fromhex('4d5a90000300000004000000ffff0000') #DOS Header
        data += bytes.fromhex('b8000000000000004000000000000000')
        data += bytes.fromhex('00000000000000000000000000000000')
        data += bytes.fromhex('00000000000000000000000080000000')
        data += bytes.fromhex('0e1fba0e00b409cd21b8014ccd21') #DOS Stub
        data += 'This program cannot be run in DOS mode'.encode('ascii')
        data += bytes.fromhex('2e0d0d0a2400000000000000')
        data += bytes.fromhex('50450000') #NT Magic
        data += bytes.fromhex('04c0') #Linker version
        data += random.randbytes(int(max(0,(size*1024*1024)-116))) #"Contents"
    filename = '{}{}'.format(filename,extension)
    with open(filename,'wb') as f:
        f.write(data)
    return os.path.getsize(filename)/1024/1024 #Output is MiB

def gen_archive(filename, size, type=None):
    #TODO
    return os.path.getsize(filename)/1024/1024 #Output is MiB

def create_file(type, file_size, base_path):
    '''Make decisions for creating a file on the filesystem.'''
    filename = '{}{}'.format(base_path,gen_name())
    print(type, file_size, filename)
    ret = 0
    if type == 'DOCUMENT':
        ret =  random.choice([gen_docx,gen_xlsx,gen_text])(filename,file_size)
    if type == 'CODE':
        ret =  gen_code(filename,file_size)
    if type == 'BINARY':
        ret =  random.choice([gen_binary,gen_exe])(filename,file_size)
#    if type == 'ARCHIVE':
#        ret =  gen_archive(filename,file_size)
    print(ret)
    return ret

root={q:1 for q in types}
root['NEW']=10
def generate_folder(path):
    name = gen_name()
    path[name]={q:1 for q in types}
    path[name]['NEW']=1
    return name

def select_folder_and_type(path,base=''):
    keys = list(path.keys())
    counts = [path[q]if type(path[q]) is int else 1 for q in keys]
    s = random.sample(keys,counts=counts,k=1)[0]
    if s in types:
        path[s]+=1
        return '{}/'.format(base), s
    if s == 'NEW':
        path[s]+=1
        name = generate_folder(path)
        return select_folder_and_type(path[name],base='{}/{}'.format(base,name))
    else:
        return select_folder_and_type(path[s],base='{}/{}'.format(base,s))

def make_doc(current_size=0):
    global root
    path, type = select_folder_and_type(root)
    path =  '{}{}'.format(base_path,path) #Add any hardcoded base
    os.makedirs(path,exist_ok=True)
    size = min(random.expovariate(lambd),max_size)
    current_size += create_file(type,size,path)
    return current_size

def make_docs():
    size = 0
    while size < global_max_size:
        size = make_doc(size)

if __name__ == '__main__':
    make_docs()

